/*package com.message.user.signup;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.message.user.signup.model.Signup;
import com.message.user.signup.service.impl.SignupMessagePublisherImpl;


@SpringBootTest(classes = SignupMessageControllerTest.class)
public class SignupMessageControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	SignupMessagePublisherImpl publisher;

	// This object will be initialized thanks to @AutoConfigureJsonTesters

	@LocalServerPort
	int randomServerPort;

	@Test
	public void publishMessage() throws Exception {
		String uri = "/";
		Signup sign = new Signup();
		sign.setFullName("chris johnson");
		sign.setPhone("33 5 5 6 76");

		String inputJson = mapToJson(sign);
		//publisher.message(sign);
		MvcResult mvcResult = mvc.perform(
				
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, "message is published successfully");
	}

	public String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}
}
*/