package com.message.user.signup.model;

public class Signup {
	private String fullName;
	private String phone;

	public Signup() {
		// TODO Auto-generated constructor stub
	}

	public Signup(String fullName, String phone) {
		super();
		this.fullName = fullName;
		this.phone = phone;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Signup [fullName=" + fullName + ", phone=" + phone + "]";
	}

}
