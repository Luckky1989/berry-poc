package com.message.user.signup.service;

import com.message.user.signup.model.Signup;

public interface MessagePublisher {
	public void message(Signup singup);
}
