package com.message.user.signup.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.message.user.signup.model.Signup;
import com.message.user.signup.service.MessagePublisher;
import com.message.user.signup.util.SignupValidationUtil;

@RestController
public class SignupMessageController {
	Logger log = LogManager.getLogger(SignupMessageController.class);

	@Value("${signup.rabbitmq.exchange}")
	private String exchange;

	@Autowired
	@Qualifier("signupMessagePublisherImpl")
	MessagePublisher publisher;

	@Autowired
	SignupValidationUtil util;

	@PostMapping(value = "/")
	public ResponseEntity<String> producer(@RequestBody Signup payload) {
		System.out.println("request from user is :" + payload);
		boolean validateRequest = util.validateRequest(payload);
		if (validateRequest == true) {
			publisher.message(payload);
			return new ResponseEntity<>("Message Published Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Request is not proper, either missing fullname or phone",
					HttpStatus.BAD_REQUEST);
		}
	}
}
